To replicate:

```bash
docker build -t opencv_python .
docker create --name opencv_python opencv_python
docker cp opencv_python:opencv_contrib_python_headless-4.4.0.40-cp37-cp37m-linux_x86_64.whl \
  opencv_contrib_python_headless-4.4.0.40-cp37-cp37m-linux_x86_64.whl
```

Note that the codes above have not yet been tested.