FROM python:3.7
RUN pip install numpy
RUN git clone --recursive https://github.com/skvark/opencv-python.git
WORKDIR opencv-python
ENV CMAKE_ARGS "-DOPENCV_ENABLE_NONFREE=ON"
ENV ENABLE_CONTRIB 1
ENV ENABLE_HEADLESS 1
RUN pip wheel .
